﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server.MainPart
{
    static class AdditionalFuncs
    {
        public static byte[] TrimStart(byte[] buffer, ushort Count)
        {
            if (buffer != null)
            {
                byte[] NewBuffer = new byte[buffer.Length - Count];
                for (int i = (int)Count; i < buffer.Length; i++)
                    NewBuffer[i - Count] = buffer[i];
                return NewBuffer;
            }
            else
                throw new NullReferenceException();

        }

        public static string Encoding_Message(byte[] buffer)
        {
            if (buffer != null)
                return Encoding.ASCII.GetString(buffer);
            throw new NullReferenceException();
        }

        public static string FirstByteIntoString(this byte[] buffer, ushort Count)
        {
            if (Count > buffer.Length)
                throw new Exception("Count was larger than buffer length.");
            //~~~~~~~~~~~~~~~~~~~~~~
            string Str = default(string); // null
            //~~~~~~~~~~~~~~~~~~~~~~
            for (int i = 0; i < Count; i++)
                Str += buffer[i].ToString();
            return Str;
        }
        public static string PasswordIntoHash(string Password)
        {
            if (Password != null)
            {
                System.Security.Cryptography.MD5CryptoServiceProvider Gen = new System.Security.Cryptography.MD5CryptoServiceProvider();
                byte[] buffer = Gen.ComputeHash(Encoding.ASCII.GetBytes(Password));
                StringBuilder SB = new StringBuilder();
                for (int i = 0; i < buffer.Length; i++)
                    SB.Append(buffer[i].ToString("x2"));
                return SB.ToString();
            }
            else
                throw new NullReferenceException();
        }

        public static byte[] Append(this byte[] buffer, params byte[][] buffers)
        {
            List<byte> LB = new List<byte>();
            LB.AddRange((IEnumerable<byte>)buffer);
            for (int i = 0; i < buffers.Length; i++)
                LB.AddRange((IEnumerable<byte>)buffers[i]);
            return LB.ToArray();
        }
    }
}

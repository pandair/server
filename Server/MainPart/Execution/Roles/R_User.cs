﻿using System;
using System.Linq;
using Server.MainPart.Execution.Interfaces;

namespace Server.MainPart.Execution.Roles
{
    class R_User : IExecute
    {
        protected User Client;

        public R_User(User _Client) => Client = _Client;
    }
}

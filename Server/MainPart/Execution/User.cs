﻿using System;
using Objects;
using System.Net;
using System.Linq;
using Server.MainPart.Execution.Roles;
using Server.MainPart.Execution.Interfaces;

namespace Server.MainPart.Execution
{
    class User
    {
        public string NickName { get; private set; }
        public string Hash { get; private set; }
        public string Token;
        public IPEndPoint IPEP;
        public Permission Perm;
        public IExecute Execute;

        public User(string _NickName, string _Hash, string _Token, IPEndPoint _IPEP, Permission _Perm = Permission.User)
        {
            if (_NickName == null || _Hash == null)
                throw new NullReferenceException();
            NickName = _NickName;
            Hash = _Hash;
            Token = _Token;
            IPEP = _IPEP;
            Perm = _Perm;
            InstallOpportunity(Perm);
        }

        public void InstallOpportunity(Permission _Perm)
        {
            Perm = _Perm;
            switch (_Perm)
            {
                case Permission.User:
                    Execute = new R_User(this);
                    break;
                case Permission.Moderator:
                    Execute = new R_Moderator(this);
                    break;
                case Permission.Admin:
                    Execute = new R_Admin(this);
                    break;
            }
        }
    }
}

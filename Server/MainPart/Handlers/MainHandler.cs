﻿using System;
using Objects;
using Newtonsoft.Json;
using System.Threading;
using System.Collections.Generic;

namespace Server.MainPart.Handlers
{
    static class MainHandler
    {
        static Queue<byte[]> QU = new Queue<byte[]>();
        static Thread MainWorker = new Thread(MainDistributor); // костыль

        public static void AddIntoQueue(byte[] buffer)
        {
            if (buffer != null)
            {
                QU.Enqueue(buffer);
                if (MainWorker.ThreadState == ThreadState.Unstarted)
                    MainWorker.Start();
                else if (MainWorker.IsAlive == false)
                    (MainWorker = new Thread(MainDistributor)).Start();
            }
            else
                throw new NullReferenceException();
        }

        static void MainDistributor()
        {
            while (QU.Count > 0)
            {
                byte[] buffer = QU.Dequeue();
                string Destiny = buffer.FirstByteIntoString(3);
                string Message = AdditionalFuncs.Encoding_Message(AdditionalFuncs.TrimStart(buffer, 3));
                try
                {
                    switch (Destiny)
                    {
                        case "000":
                            RegOrAuth ROA = JsonConvert.DeserializeObject<RegOrAuth>(Message);
                            break;
                        case "001":
                            ROA = JsonConvert.DeserializeObject<RegOrAuth>(Message);
                            break;
                        case "100":
                            GetFlights GF = JsonConvert.DeserializeObject<GetFlights>(Message);
                            break;
                        case "102":
                            ReservationATicket RAT = JsonConvert.DeserializeObject<ReservationATicket>(Message);
                            break;
                        case "500":
                            AddFlight AF = JsonConvert.DeserializeObject<AddFlight>(Message);
                            break;
                        case "501":
                            RemoveFlight RF = JsonConvert.DeserializeObject<RemoveFlight>(Message);
                            break;
                        case "502":
                            //AllFlight where is only token
                            break;
                        case "600":
                            SetPerm SP = JsonConvert.DeserializeObject<SetPerm>(Message);
                            break;
                        case "601":
                            ChangePassword CP = JsonConvert.DeserializeObject<ChangePassword>(Message);
                            break;
                    }
                }
                catch (Exception e) { Console.WriteLine(e.Message); }
            }
        }
    }
}

﻿using System;
using System.Net;
using System.Net.Sockets;

namespace Server.MainPart
{
    sealed class Listener
    {
        public static Socket Listen = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp); // use this
        private ushort _Port;

        /// <summary>
        /// Will be set up default port 25565.
        /// </summary>
        public Listener()
        {
            _Port = 25565;
            Listen.Bind(new IPEndPoint(IPAddress.Any, _Port));
        }

        /// <summary>
        /// Will be set up your port.
        /// </summary>
        /// <param name="Temp">Your port.</param>
        public Listener(ushort Temp)
        {
            Port = Temp;
            Listen.Bind(new IPEndPoint(IPAddress.Any, _Port));
        }

        public ushort Port
        {
            get
            {
                return _Port;
            }
            set
            {
                if (value >= 1024 && value <= 65535)
                    _Port = value;
                else
                    throw new Exception($"Out of range. Your number {value}");
            }
        }

        public void StartListen()
        {
            byte[] buffer;
            int Count = new int();

            while (true)
            {
                buffer = new byte[Listen.ReceiveBufferSize];
                Count = Listen.Receive(buffer);
                Array.Resize<byte>(ref buffer, Count);
                Handlers.MainHandler.AddIntoQueue(buffer);
            }

        }

    }
}
